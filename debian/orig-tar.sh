#!/bin/sh -e

VERSION=$2
TAR=../aether_$VERSION.orig.tar.gz
DIR=aether-$VERSION

tar xzf $3
mv sonatype-sonatype-aether-* $DIR
GZIP=--best tar czf $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR
